﻿/// <summary>
/// Download our games at https://bit.ly/levenight or Google Playstore
/// </summary>

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Levenight.VirtualJoystick
{
    [RequireComponent (typeof (RectTransform))]
    public class MobileJoystick : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler
    {
        private RectTransform _container;
        private Vector2 _axis = Vector2.zero;

        [Tooltip ("Key to call joystick axis in script")]
        public string joystickName;

        [Tooltip ("UI Image that used to move the joystick")]
        public RectTransform joystick;

        [Tooltip ("UI Text to show the value of joystick movement axis")]
        public Text debugInfo;

        [Header ("Alternative Axis Keys")]

        [Tooltip ("Alternative Horizontal Axis for real input hardware")]
        public string horizontalAlternative = "Horizontal";

        [Tooltip ("Alternative Vertical Axis for real input hardware")]
        public string verticalAlternative = "Vertical";

        [Header ("Optional Configuration")]

        [Tooltip ("Scale joystick to this value when it got pressed")]
        [Range (0f, 2f)] public float pressedScale = 1f;

        private void Awake ()
        {
            _container = GetComponent<RectTransform> ();

            if (joystickName.Length <= 0)
            {
                Debug.LogWarning ("Joystick name was empty string!");
            }

            MobileInput.instance.AddJoystick (this);
        }

        public void OnDrag (PointerEventData pointer)
        {
            Vector2 movement = Vector2.zero;

            RectTransformUtility.ScreenPointToLocalPointInRectangle (
                _container, pointer.position, pointer.pressEventCamera, out movement
            );

            movement.x /= _container.sizeDelta.x;
            movement.y /= _container.sizeDelta.y;

            float x = _container.pivot.x == 1f ? movement.x * 2f + 1f : movement.x * 2f - 1f;
            float y = _container.pivot.y == 1f ? movement.y * 2f + 1f : movement.y * 2f - 1f;

            _axis = new Vector2 (x, y);
            _axis = _axis.magnitude >= 1f ? _axis.normalized : _axis;

            joystick.anchoredPosition = new Vector2 (
                _axis.x * (_container.sizeDelta.x / 3f),
                _axis.y * (_container.sizeDelta.y / 3f)
            );

            UpdateDebugInfo ();
        }

        public void OnPointerDown (PointerEventData pointer)
        {
            joystick.transform.localScale = Vector3.one * pressedScale;

            OnDrag (pointer);
        }

        public void OnPointerUp (PointerEventData pointer)
        {
            joystick.transform.localScale = Vector3.one;

            _axis = Vector2.zero;
            joystick.anchoredPosition = Vector2.zero;

            UpdateDebugInfo ();
        }

        public float GetAxis (string direction)
        {
            if (!MobileInput.instance.enableMobileInput)
            {
                return Input.GetAxisRaw (direction);
            }

            if (direction == horizontalAlternative)
            {
                return _axis.x;
            }

            if (direction == verticalAlternative)
            {
                return _axis.y;
            }

            Debug.LogError ("No Axis: '" + direction + "' Direction!");
            return 0f;
        }

        public int GetAxisRaw (string direction)
        {
            if (!MobileInput.instance.enableMobileInput)
            {
                return (int) Input.GetAxisRaw (direction);
            }

            if (direction == horizontalAlternative)
            {
                return (int) Mathf.Round (_axis.x);
            }

            if (direction == verticalAlternative)
            {
                return (int) Mathf.Round (_axis.y);
            }

            Debug.LogError ("No Axis: '" + direction + "' Direction!");
            return 0;
        }

        private void UpdateDebugInfo ()
        {
            if (!MobileInput.instance.showDebug)
            {
                return;
            }

            if (debugInfo == null)
            {
                Debug.LogWarning ("Add UI Text as child of this game object and assign to Debug Info variable to show debug!");
                return;
            }

            debugInfo.text = "Axis: " + GetAxis ("Horizontal") + ", " + GetAxis ("Vertical") + "\n" +
                             "Axis Raw: " + GetAxisRaw ("Horizontal") + ", " + GetAxisRaw ("Vertical");
        }
    }
}