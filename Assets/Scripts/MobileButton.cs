﻿/// <summary>
/// Download our games at https://bit.ly/levenight or Google Playstore
/// </summary>

using UnityEngine;
using UnityEngine.EventSystems;

namespace Levenight.VirtualJoystick
{
    public class MobileButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        private bool _isPressed;

        private int _lastPressedFrame = -4;
        private int _lastReleasedFrame = -4;

        [Tooltip ("Key to call button in script")]
        public string buttonName;

        [Tooltip ("Alternative button when mobile input has not been used")]
        public KeyCode buttonAlternative = KeyCode.L;

        [Header ("Optional Configuration")]

        [Tooltip ("Scale button to this value when it got pressed")]
        [Range (0f, 2f)] public float pressedScale = 0.85f;

        public void Awake ()
        {
            if (buttonName.Length <= 0)
            {
                Debug.LogWarning ("Button name was empty string!");
            }

            MobileInput.instance.AddButton (this);
        }

        public void OnPointerDown (PointerEventData pointer)
        {
            if (_isPressed)
            {
                return;
            }

            transform.localScale = Vector3.one * pressedScale;

            _isPressed = true;
            _lastPressedFrame = Time.frameCount;

            if (MobileInput.instance.showDebug)
            {
                Debug.Log ("<color=#6464ff>" + buttonName + " on pressed!</color>");
            }
        }

        public void OnPointerUp (PointerEventData pointer)
        {
            _isPressed = false;
            _lastReleasedFrame = Time.frameCount;

            transform.localScale = Vector3.one;

            if (MobileInput.instance.showDebug)
            {
                Debug.Log ("<color=#ffffff>" + buttonName + " on released!</color>");
            }
        }

        public bool GetButton
        {
            get
            {
                if (MobileInput.instance.enableMobileInput)
                {
                    return _isPressed;
                }

                return Input.GetKey (buttonAlternative);
            }
        }

        public bool GetButtonDown
        {
            get
            {
                if (MobileInput.instance.enableMobileInput)
                {
                    return _lastPressedFrame - Time.frameCount == -1;
                }

                return Input.GetKeyDown (buttonAlternative);
            }
        }

        public bool GetButtonUp
        {
            get
            {
                if (MobileInput.instance.enableMobileInput)
                {
                    return _lastReleasedFrame == Time.frameCount - 1;
                }

                return Input.GetKeyUp (buttonAlternative);
            }
        }
    }
}