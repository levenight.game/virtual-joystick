﻿/// <summary>
/// Download our games at https://bit.ly/levenight or Google Playstore
/// </summary>

using System.Collections.Generic;
using UnityEngine;

namespace Levenight.VirtualJoystick
{
    public sealed class MobileInput : MonoBehaviour
    {
        private static MobileInput _instance = null;
        public static MobileInput instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<MobileInput> ();
                }

                if (_instance == null)
                {
                    Debug.LogError ("Mobile Input Control has not been assigned to this scene!");
                }

                return _instance;
            }
        }

        public Dictionary<string, MobileJoystick> registeredJoysticks = new Dictionary<string, MobileJoystick> ();
        public Dictionary<string, MobileButton> registeredButtons = new Dictionary<string, MobileButton> ();

        [Tooltip ("Enable or disable mobile touch control")]
        public bool enableMobileInput = true;
        private bool _enableMobileInput;

        [Tooltip ("Show debug for virtual button and joystick")]
        public bool showDebug = true;
        private bool _showDebug;

        private void Start ()
        {
            _enableMobileInput = !enableMobileInput;
            _showDebug = !showDebug;

            OnChangeEnableMobileInput ();
            OnChangeShowDebug ();
        }

        private void LateUpdate ()
        {
            OnChangeEnableMobileInput ();
            OnChangeShowDebug ();
        }

        private void OnChangeEnableMobileInput ()
        {
            if (_enableMobileInput != enableMobileInput)
            {
                foreach (MobileJoystick joystick in registeredJoysticks.Values)
                {
                    joystick.gameObject.SetActive (enableMobileInput);
                }

                foreach (MobileButton button in registeredButtons.Values)
                {
                    button.gameObject.SetActive (enableMobileInput);
                }

                _enableMobileInput = enableMobileInput;
            }
        }

        private void OnChangeShowDebug ()
        {
            if (_showDebug != showDebug)
            {
                foreach (MobileJoystick joystick in registeredJoysticks.Values)
                {
                    joystick.debugInfo.enabled = showDebug;
                }

                _showDebug = showDebug;
            }
        }

        public void AddButton (MobileButton button)
        {
            if (registeredButtons.ContainsKey (button.buttonName))
            {
                Debug.LogError ("Duplicated Mobile Button Name!");
                return;
            }

            registeredButtons.Add (button.buttonName, button);
        }

        public void AddJoystick (MobileJoystick joystick)
        {
            if (registeredJoysticks.ContainsKey (joystick.joystickName))
            {
                Debug.LogError ("Duplicated Mobile Joystick Name!");
                return;
            }

            registeredJoysticks.Add (joystick.joystickName, joystick);
        }

        public bool GetButtonDown (string buttonName)
        {
            return registeredButtons[buttonName].GetButtonDown;
        }

        public bool GetButton (string buttonName)
        {
            return registeredButtons[buttonName].GetButton;
        }

        public bool GetButtonUp (string buttonName)
        {
            return registeredButtons[buttonName].GetButtonUp;
        }

        public float GetAxis (string joystickName, string direction)
        {
            return registeredJoysticks[joystickName].GetAxis (direction);
        }

        public float GetAxisRaw (string joystickName, string direction)
        {
            return registeredJoysticks[joystickName].GetAxisRaw (direction);
        }
    }
}