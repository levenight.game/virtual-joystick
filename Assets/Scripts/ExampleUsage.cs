﻿/// <summary>
/// Download our games at https://bit.ly/levenight or Google Playstore
/// </summary>

using UnityEngine;
using Levenight.VirtualJoystick;

public class ExampleUsage : MonoBehaviour
{
    private Vector3 _originPosition;
    private Vector2 _axis;

    public bool digitalOutput;

    private void Start ()
    {
        _originPosition = transform.position;
    }

    private void Update ()
    {
        /// <summary>
        /// Used for Analog Output
        /// </summary>
        if (!digitalOutput)
        {
            _axis.x = MobileInput.instance.GetAxis ("Movement", "Horizontal");
            _axis.y = MobileInput.instance.GetAxis ("Movement", "Vertical");
        }

        /// <summary>
        /// Used for Digital Output
        /// </summary>
        else
        {
            _axis.x = MobileInput.instance.GetAxisRaw ("Movement", "Horizontal");
            _axis.y = MobileInput.instance.GetAxisRaw ("Movement", "Vertical");
        }

        transform.position = _originPosition + (Vector3) _axis;

        if (MobileInput.instance.GetButtonDown ("Attack"))
        {
            transform.eulerAngles = Vector3.down * 180f;
        }

        if (MobileInput.instance.GetButtonUp ("Attack"))
        {
            transform.eulerAngles = Vector3.zero;
        }
    }
}