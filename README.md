![Levenight_-_Virtual_Joystick](/uploads/71c1568af2e25bd31d75347ec8acb15e/Levenight_-_Virtual_Joystick.png)

<h1>Features</h1>

1.  Support for Multiple Joysticks and also Multiple Buttons.
2.  Easy to toggle whether you wanna use Mobile Input or any supported device inputs such as keyboard, mouse, joystick, etc. only with single function call.
3.  Run the Example at Demo Scene to try it.
4.  Drag & Drop from Prefabs to easily add them.
5.  User Friendly Configuration.
6.  Free to edit for more features.

<h1>How to Use</h1>

1.  Import the **[Unity Package](https://gitlab.com/levenight.game/virtual-joystick/raw/master/Levenight%20-%20Virtual%20Joystick.unitypackage)** to your project.
2.  Add the **Mobile Input Control** prefab from **Levenight/Virtual Joystick/Prefabs/Mobile Input Control** to your scene hierarchy (this is the important object that will manage all the virtual joystick and buttons.
3.  You must create a Canvas object if you haven't create it before, in your scene. This will be required for placing the virtual joystick and buttons.
4.  Then, you can add the **Example Joystick** prefab from **Levenight/Virtual Joystick/Prefabs/Example Joystick** as much as you need to your Canvas, this will be your virtual joystick input.
5.  You can also add the **Example Button** prefab from **Levenight/Virtual Joystick/Prefabs/Example Button** as much as you need to your Canvas, and this will be your virtual button inputs.
6.  Use the namespace at the script where you need to use the Virtual Joystick.
```csharp
using Levenight.VirtualJoystick;
```
7.  Now, you're ready to use the virtual joystick functions!
8.  Look the **Demo** scene at **Levenight/Virtual Joystick/Scenes/Demo** to test the example virtual joystick.

<h1>Available Function</h1>

| Return Type | Name | Description |
| :-------: | :------- | :------- |
| *float* | MobileInput.instance.GetAxis (*string* **joystickName**, *string* **direction**) | Returns the analog axis value (from -1 until 1, include decimal values) from the joystick identified by **joystickName**, fill the **direction** with "Horizontal" or "Vertical" |
| *float* | MobileInput.instance.GetAxisRaw (*string* **joystickName**, *string* **direction**) | Returns the digital axis value (only -1, 0, or 1) from the joystick with identified by **joystickName**, fill the **direction** with "Horizontal" or "Vertical" |
| *bool* | MobileInput.instance.GetButtonDown (*string* **buttonName**) | Returns *true* during the frame the user pressed down the virtual button identified by **buttonName** |
| *bool* | MobileInput.instance.GetButton (*string* **buttonName**) | Returns *true* when a button with **buttonName** has been pressed and not released |
| *bool* | MobileInput.instance.GetButtonUp (*string* **buttonName**) | Returns *true* the first frame the user releases the virtual button identified by **buttonName** |

<h1>Inspector Attributes</h1>
<h4>Mobile Input</h4>  

  -  (*bool*) **Enable Mobile Input**: Check to use the virtual joystick or uncheck to use the alternative axis or keys from all supported device inputs (keyboard, mouse, joystick, etc).
  -  (*bool*) **Show Debug**: Show the visual debug for joystick axis value (default on the top of virtual joystick) at the screen and print the debug console whether the virtual button has been pressed or released.

<h4>Mobile Joystick</h4>

  -  (*string*) **Joystick Name**: Name that required to identify the joystick.
  -  (*UI.RectTransform*) **Joystick**: Rect Transform to handle the input axis movement.
  -  (*UI.Text*) **Debug Info**: Text to display the debug information for joystick
  -  (*string*) **Horizontal Alternative**: Alternative x axis that used to replace the return value of virtual joystick 
when the mobile input has been disabled.
  -  (*string*) **Vertical Alternative**: Alternative y axis that used to replace the return value of virtual joystick 
when the mobile input has been disabled.
  - (*float*) **Pressed Scale**: Scaling that will be applied to joystick handler when the joystick was being pressed.

<h4>Mobile Button</h4>

  -  (*string*) **Button Name**: Name that required to identify the button.
  -  (*Keycode*) **Button Alternative**: Alternative device keys that used to trigger the virtual button function when the mobile input has been disabled.
  -  (*float*) **Pressed Scale**: Scaling that will be applied to button object when it was being pressed.

<h4>Dont't forget to download our games at [Google Play Store](https://bit.ly/levenight)!</h4>
<h1>Levenight</h1>
levenight.game@gmail.com